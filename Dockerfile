FROM rust:1.66-slim-buster

WORKDIR /app

ADD . .

RUN chmod +x build.sh
RUN ./build.sh

ENTRYPOINT [ "./target/release/rpi_fan_control" ]

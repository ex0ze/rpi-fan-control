# RPi Fan Control

[![Build Status](https://gitlab.com/ex0ze/rpi-fan-control/badges/master/pipeline.svg)](https://gitlab.com/ex0ze/rpi-fan-control)

RPi fan control is a service to monitor Raspberry Pi temperatures and turn on/off GPIO fan according to them.

## Guide

1. Install Rust compiler (through rustup or your distro package manager)
2. Run
   ```sh
   ./build.sh
   ```
   it will build and strip RPi Fan Control
   3. Run
   ```sh
   ./install.sh
   ```
   it will:
   * Copy release executable (from step 2) to `/usr/local/bin`
   * Copy `rpi_fan_control.service` SystemD unit file to `/etc/systemd/system` folder
   * Enable `rpi_fan_control.service` now and after every restart

## Logs
To show RPi Fan Control logs, you can run
```sh
sudo systemctl status rpi_fan_control.service
```

## Configuration
There are several command line arguments available:
* `--enable-fan-on-temp` - override temp (in Celsius) on which fan will be enabled
* `--disable-fan-on-temp` - override temp (in Celsius) on which fan will be disabled
* `--poll-interval-ms` - override interval (milliseconds) of temp polling
* `--rpi-fan-pin` - override GPIO pin number of fan

You can pass these options in `rpi_fan_control.service` before install or through any editor in `/etc/systemd/system/rpi_fan_control.service` - e.g.
```
ExecStart=/usr/local/bin/rpi_fan_control --enable-fan-on-temp=60 --disable-fan-on-temp=50 --poll-interval-ms=1000
```

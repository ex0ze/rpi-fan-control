#!/bin/bash

set -e

echo "Building RPi Fan Control..."
cargo build --release

echo "Stripping RPi Fan Control executable..."
strip target/release/rpi_fan_control

echo "Done!"

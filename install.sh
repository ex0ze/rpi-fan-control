#!/bin/bash

set -e

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

if [ ! -f target/release/rpi_fan_control ]
then
    echo "Can't find release build of RPi Fan Control. Launch build.sh first"
    exit
fi

echo "Copying rpi_fan_control to /usr/local/bin/"
cp target/release/rpi_fan_control /usr/local/bin/

echo "Copying systemd unit file to /etc/systemd/system/"
cp rpi_fan_control.service /etc/systemd/system/

echo "Reloading systemd daemons"
systemctl daemon-reload

echo "Enabling rpi fan control launch on system start"
systemctl enable rpi_fan_control

echo "Starting rpi fan control"
systemctl start rpi_fan_control

echo "Done!"

use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct CommandLineArgs {
    /// On which temp enable fan
    #[arg(long, default_value_t = 65)]
    pub enable_fan_on_temp: i64,

    /// On which temp disable fan
    #[arg(long, default_value_t = 55)]
    pub disable_fan_on_temp: i64,

    /// Interval of temp polling
    #[arg(long, default_value_t = 5000)]
    pub poll_interval_ms: u64,

    /// GPIO pin number of fan
    #[arg(long, default_value_t = 17)]
    pub rpi_fan_pin: u16,
}

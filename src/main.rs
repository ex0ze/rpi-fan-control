mod command_line_args;

use std::{thread::sleep, time::Duration};

use clap::Parser;
use command_line_args::CommandLineArgs;
use gpio::{sysfs::SysFsGpioOutput, GpioOut};
use tracing::{info, warn};

fn get_current_temp() -> anyhow::Result<i64> {
    Ok(
        std::fs::read_to_string("/sys/class/thermal/thermal_zone0/temp")?
            .trim()
            .parse::<i64>()?
            / 1000,
    )
}

fn main() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::FmtSubscriber::new();
    tracing::subscriber::set_global_default(subscriber)?;

    let cl_args = CommandLineArgs::parse();

    let mut gpio_fan = SysFsGpioOutput::open(cl_args.rpi_fan_pin)
        .expect("Couldn't open Raspberry Pi GPIO fan pin");

    gpio_fan.set_high()?;
    let mut fan_enabled = true;

    loop {
        sleep(Duration::from_millis(cl_args.poll_interval_ms));

        match get_current_temp() {
            Ok(temp) => {
                if temp > cl_args.enable_fan_on_temp && !fan_enabled {
                    gpio_fan.set_high()?;
                    fan_enabled = true;
                    info!("Temp reached {}°C, fan enabled", temp);
                } else if temp < cl_args.disable_fan_on_temp && fan_enabled {
                    gpio_fan.set_low()?;
                    fan_enabled = false;
                    info!("Temp reached {}°C, fan disabled", temp);
                }
            }
            Err(e) => {
                warn!("Error while getting temp: {:?}", e);
            }
        }
    }
}
